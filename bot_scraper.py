import os
import time

import pandas as pd

import requests
from selenium.webdriver.common.by import By

from browser import init_web_driver
from common import urls, hosts, scrapers, common_keys, helpers
from common.params import *
from datetime import datetime
from bs4 import BeautifulSoup


class BotScraper:

    def __init__(self, params, proxy=None):
        self.params = params
        self.proxy = proxy
        self.scrapers_dict = {
            scrapers.TDC: self.taux_de_change,
            scrapers.CDT: self.courbe_des_taux,
            scrapers.TDT: self.taux_depot_a_terme,
            scrapers.TCC: self.taux_compte_sur_carnet,
            scrapers.TID: self.taux_interet_debiteur,
            scrapers.IISJ: self.inflation_et_inflation_sous_jacente,
            scrapers.AOR: self.avoirs_officielles_de_reverses,
            scrapers.IB: self.intervention_bam,
            scrapers.SM: self.statistiques_monetaire,
            scrapers.SEF: self.statistique_economique_financier,
            scrapers.IMEE: self.indicateur_mensuels_echanges_exterieurs,
            scrapers.CDT_UMOA: self.courbe_des_taux_umoa,
            scrapers.CDT_CEMAC: self.courbe_des_taux_cemac,
            scrapers.OPMT: self.operation_politique_monetaire_tunisie,
            scrapers.SM_BCT_TUNISIE: self.situation_mensuelle_bct_tunisie,
            scrapers.PTIT: self.principaux_taux_interet_tunisie,
            scrapers.BPT: self.balance_paiement_tunisie,
            scrapers.CDT_EGY_T_BILLS: self.courbe_de_taux_egypte_bills,
            scrapers.CDT_EGY_T_BONDS: self.courbe_de_taux_egypte_bounds,
            scrapers.CDT_EGY_FRD: self.courbe_de_taux_egypte_frd,
            scrapers.DIH: self.daily_interbank_historical_egypte,
            scrapers.DTPYCR: self.daily_treasury_par_yield_curve_rates,
            scrapers.GETWAY_B_PCEC: self.getway_balance_pcec,
            scrapers.GETWAY_DAT: self.getway_dat,
        }

    def get(self, url):
        if self.proxy not in [{}, None]:
            return requests.get(url, proxies=self.proxy, verify=False)
        else:
            return requests.get(url)

    def cours_de_change_et_courbe_des_taux(self, scraper_name: str = scrapers.TDC):

        assert scraper_name in [scrapers.TDC, scrapers.CDT]

        course_date = self.params.get(scraper_name).get(TDC.DATE if scraper_name == scrapers.TDC else CDT.DATE)
        output_path = self.params.get(scraper_name).get(
            TDC.OUTPUT_PATH if scraper_name == scrapers.TDC else CDT.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scraper_name + '\\'
        try:
            placeholder = urls.taux_de_change if scraper_name == scrapers.TDC else urls.courbe_des_taux
            if course_date == '':
                url = placeholder.split('?')[0]
                course_date = datetime.today()
            else:
                course_date = datetime.strptime(course_date, "%d/%m/%Y")
                url = placeholder.format("{0:02}".format(course_date.day),
                                         "{0:02}".format(course_date.month),
                                         course_date.year)

            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            table = soup.find('div', attrs={'class': 'block-table'})
            if table is not None and table.text.__str__().__contains__(common_keys.AUCUN_ENREGISTREMENT_TROUVE):
                return common_keys.FILE_NOT_FOUND
            else:
                csv_endpoint = soup.find('a', attrs={'class': 'link-CSV'})
                csv_table = self.get(hosts.HOST_BKAM + csv_endpoint.attrs.get('href'))
                with open('{}{}_{}.csv'.format(output_path, scraper_name, course_date.strftime("%Y-%m-%d")),
                          'w') as file:
                    file.write(csv_table.text)
                return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED


    def taux_de_change(self):
        return self.cours_de_change_et_courbe_des_taux(scraper_name=scrapers.TDC)

    def courbe_des_taux(self):
        return self.cours_de_change_et_courbe_des_taux(scraper_name=scrapers.CDT)

    def taux_depot_a_terme(self):

        month = self.params.get(scrapers.TDT).get(TDT.MONTH)
        year = self.params.get(scrapers.TDT).get(TDT.YEAR)
        output_path = self.params.get(scrapers.TDT).get(TDT.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.TDT + '\\'

        try:
            placeholder = urls.taux_depot_a_terme

            if month == '' or year == '':
                today = datetime.today()
                year, month = today.year, today.month
                url = placeholder.split('?')[0]
            else:
                url = placeholder.format(month, year)

            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            table = soup.find('div', attrs={'class': 'block-table'})
            if table is not None and table.text.__str__().__contains__(common_keys.AUCUN_ENREGISTREMENT_TROUVE):
                return common_keys.FILE_NOT_FOUND
            else:
                csv_endpoint = soup.find('a', attrs={'class': 'link-CSV'})
                csv_table = self.get(hosts.HOST_BKAM + csv_endpoint.attrs.get('href'))
                with open('{}{}_{}-{}.csv'.format(output_path, scrapers.TDT, month, year), 'w') as file:
                    file.write(csv_table.text)
                return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def taux_compte_sur_carnet(self):
        start_date = datetime.strptime(self.params.get(scrapers.TCC).get(TCC.START_DATE), "%d/%m/%Y")
        end_date = datetime.strptime(self.params.get(scrapers.TCC).get(TCC.END_DATE), "%d/%m/%Y")
        output_path = self.params.get(scrapers.TCC).get(TCC.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.TDT + '\\'
        try:
            placeholder = urls.taux_compte_sur_carnet
            if start_date == '' or end_date == '':
                url = placeholder.split('?')[0]
            else:
                url = placeholder.format(
                    "{0:02}".format(start_date.day),
                    "{0:02}".format(start_date.month),
                    start_date.year,
                    "{0:02}".format(end_date.day),
                    "{0:02}".format(end_date.month),
                    end_date.year)

            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            table = soup.find('div', attrs={'class': 'block-table'})
            if table.text.__str__().__contains__(common_keys.AUCUN_ENREGISTREMENT_TROUVE):
                return common_keys.FILE_NOT_FOUND
            else:
                csv_endpoint = soup.find('a', attrs={'class': 'link-CSV'})
                csv_table = self.get(hosts.HOST_BKAM + csv_endpoint.attrs.get('href'))
                with open('{}{}_{}_{}.csv'.format(output_path, scrapers.TCC, start_date.strftime("%Y-%m-%d"),
                                                  end_date.strftime("%Y-%m-%d")), 'w') as file:
                    file.write(csv_table.text)
                return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def taux_interet_debiteur(self):
        start_year = self.params.get(scrapers.TID).get(TID.START_DATE)
        end_year = self.params.get(scrapers.TID).get(TID.END_DATE)
        trimestre = self.params.get(scrapers.TID).get(TID.TRIMESTRE)
        output_path = self.params.get(scrapers.TID).get(TID.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.TID + '\\'

        try:
            url = urls.taux_interet_debteur.format(TID.ENCODE_T[trimestre], start_year, end_year)
            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            csv_endpoint = soup.find('a', attrs={'class': 'link-CSV'})
            csv_table = self.get(hosts.HOST_BKAM + csv_endpoint.attrs.get('href'))
            if len(csv_table.text.split('\n')) > 3:
                with open('{}{}_{}_{}_{}.csv'.format(output_path, scrapers.TID, start_year, end_year, trimestre), 'w',
                          encoding="utf-8") as file:
                    file.write(csv_table.text)
                return common_keys.DOWNLOAD_SUCCESS
            else:
                return common_keys.FILE_NOT_FOUND

        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def inflation_et_inflation_sous_jacente(self):

        output_path = self.params.get(scrapers.IISJ).get(IISJ.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.IISJ + '\\'

        try:
            url = urls.inflation_et_inflation_sous_jacente
            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            csv_endpoint = soup.find('div', attrs={'class': 'block-link'}).find('a')
            csv_table = self.get(hosts.HOST_BKAM + csv_endpoint.attrs.get('href'))
            with open('{}{}_{}.xlsx'.format(output_path, scrapers.IISJ, datetime.today().strftime("%Y-%m-%d")),
                      'wb') as file:
                file.write(csv_table.content)
            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            # print(e.__str__())
            # return common_keys.DOWNLOAD_FAILED
            raise e

    def avoirs_officielles_de_reverses(self):

        output_path = self.params.get(scrapers.AOR).get(AOR.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.AOR + '\\'

        try:
            url = urls.avoirs_officielles_de_reverses
            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            csv_endpoint = soup.find('a', attrs={'class': 'link-pdf xlsx'})
            csv_table = self.get(hosts.HOST_BKAM + csv_endpoint.attrs.get('href'))
            with open('{}{}_{}.xlsx'.format(output_path, scrapers.AOR, datetime.today().strftime("%Y-%m-%d")),
                      'wb') as file:
                file.write(csv_table.content)
            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            # print(e.__str__())
            # return common_keys.DOWNLOAD_FAILED
            raise e

    def intervention_bam(self):
        start_date = self.params.get(scrapers.IB).get(IB.START_DATE)
        end_date = self.params.get(scrapers.IB).get(IB.END_DATE)
        instrument = self.params.get(scrapers.IB).get(IB.INSTRUMENT)

        assert instrument in [common_keys.AVANCES_7_JOURS, common_keys.REPRISE_DE_LIQUIDITE_7_JOURS, '']

        try:
            placeholder = urls.intervention_bam

            if instrument == '' or start_date == '' or end_date == '':
                start_date = datetime.strptime("31/12/1983", "%d/%m/%Y")
                end_date = datetime.today()
                url = placeholder.split('?')[0]
            else:
                start_date = datetime.strptime(start_date, "%d/%m/%Y")
                end_date = datetime.strptime(end_date, "%d/%m/%Y")
                url = placeholder.format(
                    124 if instrument == common_keys.AVANCES_7_JOURS else 125,
                    "{0:02}".format(start_date.day),
                    "{0:02}".format(start_date.month),
                    start_date.year,
                    "{0:02}".format(end_date.day),
                    "{0:02}".format(end_date.month),
                    end_date.year)
            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            table = soup.find('div', attrs={'class': 'block-table'})
            if table.text.__str__().__contains__(common_keys.AUCUN_ENREGISTREMENT_TROUVE):
                return common_keys.FILE_NOT_FOUND
            else:
                csv_endpoint = soup.find('a', attrs={'class': 'link-CSV'})
                csv_table = self.get(hosts.HOST_BKAM + csv_endpoint.attrs.get('href'))
                with open('{}{}_{}_{}.csv'.format(common_keys.PARENT_PATH, scrapers.IB, start_date.strftime("%Y-%m-%d"),
                                                  end_date.strftime("%Y-%m-%d")), 'w') as file:
                    file.write(csv_table.text)
                return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def statistiques_monetaire(self):

        output_path = self.params.get(scrapers.SM).get(SM.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.SM + '\\'
        try:
            url = urls.statistiques_monitaire
            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            xlsx_containers = soup.find_all('a', attrs={'class': 'link-pdf xlsx'})
            for xlsx_endpoint in xlsx_containers:
                print(xlsx_endpoint.text)
                csv_table = self.get(hosts.HOST_BKAM + xlsx_endpoint.attrs.get('href'))
                with open('{}/{}.xlsx'.format(output_path, xlsx_endpoint.text), 'wb') \
                        as file:
                    file.write(csv_table.content)

            xls_containers = soup.find_all('a', attrs={'class': 'link-pdf xls'})
            for xls_endpoint in xls_containers:
                print(xls_endpoint.text)
                csv_table = self.get(hosts.HOST_BKAM + xls_endpoint.attrs.get('href'))
                with open('{}{}.xls'.format(output_path, xls_endpoint.text), 'wb') as file:
                    file.write(csv_table.content)
            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def statistique_economique_financier(self):
        output_path = self.params.get(scrapers.SEF).get(SEF.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.SEF + '\\'

        try:
            current_date = datetime.now()
            url = urls.statistique_economique_financier
            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            links = soup.find('ul', attrs={'class': 'inner show'}).find_all('a')
            for link in links[:-1]:
                xlsx_endpoint = self.get(hosts.HOST_FINANCE_GOV_MA + link.attrs.get('href'))
                file_name = link.attrs.get('href').split('/')[-1].split('.')[0]
                with open("{}{}_{}-{}.xls".format(output_path, file_name, current_date.year, current_date.month), "wb") as file:
                    file.write(xlsx_endpoint.content)
            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def indicateur_mensuels_echanges_exterieurs(self):
        month = int(self.params.get(scrapers.IMEE).get(IMEE.MONTH))
        year = int(self.params.get(scrapers.IMEE).get(IMEE.YEAR))
        output_path = self.params.get(scrapers.IMEE).get(IMEE.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.IMEE + '\\'
        assert month in range(1, 13)

        try:
            search_sentence = common_keys.INDECATOR_SEARCH_SENTENCE.format(common_keys.MONTHS.get(month), year)
            url = urls.indicateur_mensuels_echanges_exterieurs.format(search_sentence.replace(' ', '+'))
            response = self.get(url)
            soup = BeautifulSoup(response.content, "html.parser")
            results = soup.find_all('li', attrs={'class': 'pb-2 mb-2'})

            for result in results:
                if result.find('span', attrs={'class': 'grey-light'}).text.__contains__('Article') \
                        and result.find('span', attrs={'class': 'blue f18 underline'}) \
                        .find('a').text.lower().__contains__(search_sentence.lower()):
                    next_page_link = result.find('span', attrs={'class': 'blue f18 underline'}) \
                        .find('a').attrs.get('href')
                    next_page_response = self.get(hosts.OC_GO_MA + next_page_link)
                    soup = BeautifulSoup(next_page_response.content, "html.parser")
                    pdf_link = soup.find('a', attrs={'class': 'blue-dark pr-1'}).attrs.get('href')
                    pdf = self.get(pdf_link)
                    with open('{}{}_{}-{}.pdf'.format(output_path, scrapers.IMEE, year, month), 'wb') as file:
                        file.write(pdf.content)
                    return common_keys.DOWNLOAD_SUCCESS
            return common_keys.FILE_NOT_FOUND

        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def courbe_des_taux_umoa(self):
        date = datetime.strptime(self.params.get(scrapers.CDT_UMOA).get(CDT_UMOA.DATE), "%d/%m/%Y")
        output_path = self.params.get(scrapers.CDT_UMOA).get(CDT_UMOA.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.CDT_UMOA + '\\'

        try:
            url = "https://www.umoatitres.org/fr/ressources-2/courbe-des-taux/"
            response = self.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            month_containers = soup.find_all('div', attrs={'class': 'wpb_column vc_column_container vc_col-sm-3'})

            for container in month_containers:
                title = container.find('div', attrs={'class': 'wpb_text_column wpb_content_element'})
                if title is not None:
                    title = title.text.replace('\n', '')
                    if title.split(' ')[0] == common_keys.MONTHS[date.month] and title.split(' ')[-1] == str(date.year):
                        print(title)
                        links = container.find_all('a')
                        for link in links:
                            if link is not None \
                                    and date.strftime('%d.%m.%Y') == link.text.split(' ')[-1] \
                                    or date.strftime('%d %m %Y') == link.text[-10:]:
                                xlsx_url = link.attrs.get('href')
                                xlsx_url = xlsx_url if xlsx_url[:6] == 'https:' else 'https:' + xlsx_url
                                xlsx_file = self.get(xlsx_url)
                                with open('{}{}.xlsx'.format(output_path, link.text), 'wb') as file:
                                    file.write(xlsx_file.content)

                                return common_keys.DOWNLOAD_SUCCESS
            return common_keys.FILE_NOT_FOUND
        except Exception as e:
            # print(e.__str__())
            # return common_keys.DOWNLOAD_FAILED
            raise e

    def courbe_des_taux_cemac(self):

        year = self.params.get(scrapers.CDT_CEMAC).get(CDT_CEMAC.YEAR)
        month = self.params.get(scrapers.CDT_CEMAC).get(CDT_CEMAC.MONTH)
        output_path = self.params.get(scrapers.CDT_CEMAC).get(CDT_CEMAC.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.CDT_CEMAC + '\\'

        try:
            driver = init_web_driver(output_path, True)
            driver.get(urls.courbe_des_taux_cemac)

            titles_and_links = list(list())
            time.sleep(2)

            selector = driver.find_element(By.XPATH, '//*[@id="filters"]/select[4]')
            selector.send_keys(year)

            number_of_pages = driver.find_element(By.XPATH, '//*[@id="documents_contenu_cpt_paginate"]/span') \
                .find_elements(By.TAG_NAME, 'a')[-1].text

            for page_number in range(int(number_of_pages)):
                table = driver.find_element(By.ID, 'documents_contenu_cpt')
                if table is not None:
                    for line in table.find_elements(By.TAG_NAME, 'a'):
                        if line.text is not None and line.text.lower().__contains__(common_keys.MONTHS[int(month)].lower()):
                            titles_and_links.append([line.text, line.get_attribute('href')])

                next_button = driver.find_element(By.ID, 'documents_contenu_cpt_next')
                next_button.click()

            if len(titles_and_links) > 0:
                for element in titles_and_links:
                    pdf_file = self.get(element[1])
                    with open('{}{}.pdf'.format(output_path, element[0]), 'wb') as file:
                        file.write(pdf_file.content)
                        file.close()
                return common_keys.DOWNLOAD_SUCCESS
            else:
                return common_keys.FILE_NOT_FOUND
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def operation_politique_monetaire_tunisie(self):
        output_path = self.params.get(scrapers.OPMT).get(OPMT.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.OPMT + '\\'

        try:
            response = self.get(urls.operation_politique_monetaire_tunisie)
            soup = BeautifulSoup(response.content, 'html.parser')
            content = ''

            table_head = soup.find('div', attrs={'class': 'bct-header-fixed'}).find_all('td')

            file_name = "{}_{}-{}".format(scrapers.OPMT,
                                          table_head[-2].text.replace('/', '_'),
                                          table_head[-1].text.replace('/', '_'))

            content += "".join('\"' + colname.text + '\";' for colname in table_head)[:-1] + '\n'

            table_lines = soup.find('div', attrs={'class': 'bct-table-fixed'}).find_all('tbody')[-1].find_all('tr')

            for line in table_lines:
                content += "".join('\"' + col.text + '\";' for col in line.find_all('td'))[:-1] + '\n'

            with open('{}{}.csv'.format(output_path, file_name), 'w', encoding='utf-8') as file:
                file.write(content)
                file.close()
            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def situation_mensuelle_bct_tunisie(self):
        output_path = self.params.get(scrapers.SM_BCT_TUNISIE).get(SM_BCT_TUNISIE.OUTPUT_PATH)
        output_path = output_path if output_path not in ['',
                                                         None] else common_keys.PARENT_PATH + scrapers.SM_BCT_TUNISIE + '\\'

        try:
            response = self.get(urls.situation_mensuelle_bct_tunisie)
            soup = BeautifulSoup(response.content, 'html.parser')
            content = ''

            table_head = soup.find('div', attrs={'class': 'bct-header-fixed'}).find_all('td')

            file_name = "{}_{}-{}".format(scrapers.SM_BCT_TUNISIE,
                                          table_head[-2].text.replace('/', '_'),
                                          table_head[-1].text.replace('/', '_'))

            content += "".join('\"' + colname.text + '\";' for colname in table_head)[:-1] + '\n'

            table_lines = soup.find('div', attrs={'class': 'bct-table-fixed'}).find_all('tbody')[-1].find_all('tr')

            for line in table_lines:
                content += "".join('\"' + col.text + '\";' for col in line.find_all('td'))[:-1] + '\n'

            with open('{}{}.csv'.format(output_path, file_name), 'w', encoding='utf-8') as file:
                file.write(content)
                file.close()
            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def principaux_taux_interet_tunisie(self):
        output_path = self.params.get(scrapers.PTIT).get(SM_BCT_TUNISIE.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.PTIT + '\\'

        try:
            current_date = datetime.today().strftime('%Y_%m_%d')
            response = self.get(urls.principaux_taux_interet_tunisie)
            soup = BeautifulSoup(response.content, 'html.parser')

            tables = soup.find_all('table')

            try:
                table_1 = pd.read_html(str(tables[1]), thousands='')[0]
                table_1.to_csv('{}{}_(B.T.C.T)-{}.csv'.format(output_path, scrapers.PTIT, current_date), index=False, sep=";")
            except Exception as e:
                print(e.__str__())
                print('Failed to load <{}> table.'.format('(B.T.C.T)'))

            try:
                table_2 = pd.read_html(str(tables[2]), thousands='')[0]
                table_2.to_csv('{}{}_(B.T.A)-{}.csv'.format(output_path, scrapers.PTIT, current_date), index=False, sep=";")
            except Exception as e:
                print(e.__str__())
                print('Failed to load <{}> table.'.format('(B.T.A)'))

            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def getway_balance_pcec(self):
        output_path = self.params.get(scrapers.GETWAY_B_PCEC).get(GETWAY_B_PCEC.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.GETWAY_B_PCEC + '\\'
        driver = init_web_driver(common_keys.INTERMEDIATE_PATH, True)
        driver.get(urls.getway_login)

        input_login = driver.find_element(By.XPATH, "/html/body/form/div/table/tbody/tr[1]/td[2]/div/input")
        input_pwd = driver.find_element(By.XPATH,  "/html/body/form/div/table/tbody/tr[2]/td[2]/div/input")
        login_button = driver.find_element(By.XPATH,  "/html/body/form/table/tbody/tr/td[2]/div/input")

        input_login.send_keys(GETWAY_LOGIN.LOGIN)
        input_pwd.send_keys(GETWAY_LOGIN.PWD)
        login_button.click()

        driver.get(urls.getway_balance_pece)

        time.sleep(10)

        driver.close()

    def getway_dat(self):
        output_path = self.params.get(scrapers.GETWAY_DAT).get(GETWAY_DAT.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] else common_keys.PARENT_PATH + scrapers.GETWAY_DAT + '\\'
        month = self.params.get(scrapers.GETWAY_DAT).get(GETWAY_DAT.MONTH)
        year = self.params.get(scrapers.GETWAY_DAT).get(GETWAY_DAT.YEAR)

        try:
            driver = init_web_driver(common_keys.INTERMEDIATE_PATH, True)
            driver.get(urls.getway_login)

            input_login = driver.find_element(By.XPATH, "/html/body/form/div/table/tbody/tr[1]/td[2]/div/input")
            input_pwd = driver.find_element(By.XPATH, "/html/body/form/div/table/tbody/tr[2]/td[2]/div/input")
            login_button = driver.find_element(By.XPATH, "/html/body/form/table/tbody/tr/td[2]/div/input")

            input_login.send_keys(GETWAY_LOGIN.LOGIN)
            input_pwd.send_keys(GETWAY_LOGIN.PWD)
            login_button.click()

            driver.get(urls.getway_dat)

            table_body = driver.find_element(By.XPATH, '/html/body/table[2]/tbody/tr/td/table/tbody')
            table_lines = table_body.find_elements(By.TAG_NAME, 'tr')

            file_not_found = True
            for line in table_lines:
                columns = line.find_elements(By.TAG_NAME, 'td')
                if len(columns) >= 7:
                    date = columns[5].text
                    date = datetime.strptime(date[4:], "%b %d %H:%M:%S %Y")
                    if date.month == int(month) and date.year == int(year):
                        file_not_found = False
                        file_container = columns[0].find_element(By.TAG_NAME, 'a')
                        link = urls.getway_dat + file_container.get_attribute('href')
                        print(date, '\t', file_container.text)
                        text_file = self.get(link)
                        with open('{}{}.txt'.format(output_path, file_container.text), 'w', encoding='utf-8') as file:
                            file.write(text_file.text)
                            file.close()

            time.sleep(2)

            driver.close()

            return common_keys.FILE_NOT_FOUND if file_not_found else common_keys.DOWNLOAD_SUCCESS

        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def balance_paiement_tunisie(self):
        output_path = self.params.get(scrapers.BPT).get(BPT.OUTPUT_PATH)
        output_path = output_path if output_path not in ['',
                                                         None] else common_keys.PARENT_PATH + scrapers.BPT + '\\'

        try:
            current_date = datetime.today()
            quarter = (current_date.month - 1) // 3 + 1
            response = self.get(urls.balance_paiement_tunisie)
            soup = BeautifulSoup(response.content, 'html.parser')
            tables = soup.find_all('table')

            try:
                table_1_head = pd.read_html(str(tables[0]), thousands='')[0].columns
                table_1 = pd.read_html(str(tables[1]), thousands='')[0]
                table_1 = table_1.rename(
                    columns={table_1.columns[i]: table_1_head[i] for i in range(len(table_1_head))})
                table_1.to_csv('{}{}-{}_{}.csv'.format(output_path,
                                                    BPT.RECETTES,
                                                    current_date.year,
                                                    f'Trimestre_{quarter}'), index=False)
            except Exception as e:
                print(e.__str__())
                print('Failed to load <{}> table.'.format(BPT.RECETTES))
                return common_keys.DOWNLOAD_FAILED

            try:
                table_2_head = pd.read_html(str(tables[2]), thousands='')[0].columns
                table_2 = pd.read_html(str(tables[3]), thousands='')[0]
                table_2 = table_2.rename(
                    columns={table_2.columns[i]: table_2_head[i] for i in range(len(table_2_head))})
                table_2.to_csv('{}{}-{}_{}.csv'.format(output_path,
                                                    BPT.DEPENSES,
                                                    current_date.year,
                                                    f'Trimestre_{quarter}'), index=False)
            except Exception as e:
                print(e.__str__())
                print('Failed to load <{}> table.'.format(BPT.DEPENSES))
                return common_keys.DOWNLOAD_FAILED

            try:
                table_3_head = pd.read_html(str(tables[4]), thousands='')[0].columns
                table_3 = pd.read_html(str(tables[5]), thousands='')[0]
                table_3 = table_3.rename(
                    columns={table_3.columns[i]: table_3_head[i] for i in range(len(table_3_head))})
                table_3.to_csv('{}{}-{}_{}.csv'.format(output_path,
                                                       BPT.SOLDES,
                                                       current_date.year,
                                                       f'Trimestre_{quarter}'), index=False)
            except Exception as e:
                print(e.__str__())
                print('Failed to load <{}> table.'.format(BPT.SOLDES))
                return common_keys.DOWNLOAD_FAILED

            try:
                table_4 = pd.read_html(str(tables[6]), thousands='')[0]
                table_4.to_csv('{}{}-{}_{}.csv'.format(output_path,
                                                       BPT.CALENDRIER,
                                                       current_date.year,
                                                       f'Trimestre_{quarter}'))
            except Exception as e:
                print(e.__str__())
                print('Failed to load <{}> table.'.format(BPT.CALENDRIER))
                return common_keys.DOWNLOAD_FAILED

            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def courbe_de_taux_egypte_bills(self):
        output_path = self.params.get(scrapers.CDT_EGY_T_BILLS).get(CDT_EGY_T_BILLS.OUTPUT_PATH)
        output_path = output_path if output_path not in ['', None] \
            else common_keys.PARENT_PATH + scrapers.CDT_EGY_T_BILLS + '\\'
        start_year = self.params.get(scrapers.CDT_EGY_T_BILLS).get(CDT_EGY_T_BILLS.START_DATE)
        end_year = self.params.get(scrapers.CDT_EGY_T_BILLS).get(CDT_EGY_T_BILLS.END_DATE)

        try:
            helpers.reset_intermediate_folder()
            driver = init_web_driver(common_keys.INTERMEDIATE_PATH, False)

            driver.get(urls.courbe_de_taux_egypte_bills)

            input_date_from = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div[3]/'
                                                            'div[2]/div/div[2]/div/div/div/div[1]/div/ul/li[2]/div[1]/'
                                                            'div[1]/input[1]')
            input_date_to = driver.find_element(By.XPATH, '//*[@id="ctl00_ctl54_g_1eef16cc_149b_4250_b1db_366c6f7aa7e6'
                                                          '_txtToDate"]')
            excel_circle_button = driver.find_element(By.XPATH, '//*[@id="ctl00_ctl54_g_1eef16cc_149b_4250_b1db_366c6f'
                                                                '7aa7e6"]/div/ul/li[4]/div/label[2]')
            download_button = driver.find_element(By.XPATH, '//*[@id="ctl00_ctl54_g_1eef16cc_149b_4250_b1db_366c6f7aa7'
                                                            'e6_btFilters"]')

            driver.execute_script("arguments[0].removeAttribute('readonly')", input_date_from)
            driver.execute_script("arguments[0].removeAttribute('readonly')", input_date_to)

            input_date_from.click()
            input_date_from.send_keys(start_year)
            input_date_to.click()
            input_date_to.send_keys(end_year)
            excel_circle_button.click()
            download_button.click()
            time.sleep(4)
            driver.close()

            default_filename = os.listdir(common_keys.INTERMEDIATE_PATH)[0]
            os.rename(common_keys.INTERMEDIATE_PATH + default_filename,
                      common_keys.INTERMEDIATE_PATH + default_filename.replace(' ', '_'))
            default_filename = default_filename.replace(' ', '_')

            file_extension = helpers.get_file_extension(default_filename)
            filename = default_filename.replace('.' + file_extension,
                                                '_{}.{}'.format(end_year.replace('/', '-'), file_extension))

            os.system('move {} {}'.format(
                common_keys.INTERMEDIATE_PATH + default_filename,
                output_path.replace('/', '\\') + filename))

            return common_keys.DOWNLOAD_SUCCESS

        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def courbe_de_taux_egypte_bounds(self):
        output_path = self.params.get(scrapers.CDT_EGY_T_BONDS).get(CDT_EGY_T_BONDS.OUTPUT_PATH)
        output_path = output_path if output_path not in ['',
                                                         None] else common_keys.PARENT_PATH + scrapers.CDT_EGY_T_BONDS + '\\'

        try:
            response = self.get(urls.courbe_de_taux_egypte_bounds)
            soup = BeautifulSoup(response.content, 'html.parser')
            table = soup.find('div', attrs={'id': 'ctl00_ctl54_g_88fe43ec_5a6c_4313_817f_102e646f4522_TBillsResultsVw'})

            dfs = pd.read_html(str(table))

            titles = table.find_all('h3')
            current_date = datetime.today().strftime('%Y_%m_%d')

            for i in range(len(dfs)):
                dfs[i].to_csv('{}{}-{}.csv'.format(output_path, titles[i].text, current_date), index=False, sep=';')
            return common_keys.DOWNLOAD_SUCCESS
        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def courbe_de_taux_egypte_frd(self):
        output_path = self.params.get(scrapers.CDT_EGY_FRD).get(CDT_EGY_FRD.OUTPUT_PATH)
        output_path = output_path if output_path not in ['',
                                                         None] else common_keys.PARENT_PATH + scrapers.CDT_EGY_FRD + '\\'
        start_year = self.params.get(scrapers.CDT_EGY_FRD).get(CDT_EGY_FRD.START_DATE)
        end_year = self.params.get(scrapers.CDT_EGY_FRD).get(CDT_EGY_FRD.END_DATE)

        try:
            helpers.reset_intermediate_folder()
            driver = init_web_driver(common_keys.INTERMEDIATE_PATH, False)

            driver.get(urls.courbe_de_taux_egypte_frd)

            input_date_from = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div[3]/'
                                                            'div[2]/div/div[2]/div/div/div/div[1]/div/ul/li[2]/div[1]/'
                                                            'div[1]/input[1]')
            input_date_to = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div[3]/div['
                                                          '2]/div/div[2]/div/div/div/div[1]/div/ul/li[2]/div[2]/div['
                                                          '1]/input[1]')
            excel_circle_button = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div['
                                                                '3]/div[2]/div/div[2]/div/div/div/div[1]/div/ul/li['
                                                                '4]/div/label[2]')
            download_button = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div[3]/'
                                                            'div[2]/div/div[2]/div/div/div/div[1]/div/ul/li[5]/input')

            driver.execute_script("arguments[0].removeAttribute('readonly')", input_date_from)
            driver.execute_script("arguments[0].removeAttribute('readonly')", input_date_to)

            input_date_from.click()
            input_date_from.send_keys(start_year)
            input_date_to.click()
            input_date_to.send_keys(end_year)
            excel_circle_button.click()
            download_button.click()

            time.sleep(4)

            driver.close()

            default_filename = os.listdir(common_keys.INTERMEDIATE_PATH)[0]
            os.rename(common_keys.INTERMEDIATE_PATH + default_filename,
                      common_keys.INTERMEDIATE_PATH + default_filename.replace(' ', '_'))
            default_filename = default_filename.replace(' ', '_')
            file_extension = helpers.get_file_extension(default_filename)
            filename = default_filename.replace('.' + file_extension,
                                                '_{}.{}'.format(end_year.replace('/', '-'), file_extension))

            os.system('move {} {}'.format(
                common_keys.INTERMEDIATE_PATH + default_filename,
                output_path.replace('/', '\\') + filename))

            return common_keys.DOWNLOAD_SUCCESS

        except Exception as e:
            # print(e.__str__())
            # return common_keys.DOWNLOAD_FAILED
            raise e

    def daily_interbank_historical_egypte(self):
        output_path = self.params.get(scrapers.DIH).get(DIH.OUTPUT_PATH)
        output_path = output_path if output_path not in ['',
                                                         None] else common_keys.PARENT_PATH + scrapers.DIH + '\\'
        start_year = self.params.get(scrapers.DIH).get(DIH.START_DATE)
        end_year = self.params.get(scrapers.DIH).get(DIH.END_DATE)

        try:
            helpers.reset_intermediate_folder()
            driver = init_web_driver(common_keys.INTERMEDIATE_PATH, False)

            driver.get(urls.daily_interbank_historical_egypte)

            input_date_from = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div[3]/'
                                                            'div[2]/div/div[2]/div/div/div/div[1]/div/ul/li[1]/div[1]/'
                                                            'div[1]/input[1]')
            input_date_to = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div[3]/div['
                                                          '2]/div/div[2]/div/div/div/div[1]/div/ul/li[1]/div[2]/div['
                                                          '1]/input[1]')
            excel_circle_button = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div['
                                                                '3]/div[2]/div/div[2]/div/div/div/div[1]/div/ul/li['
                                                                '3]/div/label[2]')
            download_button = driver.find_element(By.XPATH, '/html/body/form/div[11]/div[4]/div[2]/div[2]/div/div[3]/'
                                                            'div[2]/div/div[2]/div/div/div/div[1]/div/ul/li[4]/input')

            driver.execute_script("arguments[0].removeAttribute('readonly')", input_date_from)
            driver.execute_script("arguments[0].removeAttribute('readonly')", input_date_to)

            input_date_from.click()
            input_date_from.send_keys(start_year)
            input_date_to.click()
            input_date_to.send_keys(end_year)
            excel_circle_button.click()
            download_button.click()

            time.sleep(4)

            driver.close()

            default_filename = os.listdir(common_keys.INTERMEDIATE_PATH)[0]
            os.rename(common_keys.INTERMEDIATE_PATH + default_filename,
                      common_keys.INTERMEDIATE_PATH + default_filename.replace(' ', '_'))
            default_filename = default_filename.replace(' ', '_')
            file_extension = helpers.get_file_extension(default_filename)
            filename = default_filename.replace('.' + file_extension,
                                                '_{}.{}'.format(end_year.replace('/', '-'), file_extension))

            os.system('move {} {}'.format(
                common_keys.INTERMEDIATE_PATH + default_filename,
                output_path.replace('/', '\\') + filename))

            return common_keys.DOWNLOAD_SUCCESS

        except Exception as e:
            print(e.__str__())
            return common_keys.DOWNLOAD_FAILED

    def daily_treasury_par_yield_curve_rates(self):
        output_path = self.params.get(scrapers.DTPYCR).get(DTPYCR.OUTPUT_PATH)
        output_path = output_path if output_path not in ['',
                                                         None] else common_keys.PARENT_PATH + scrapers.DTPYCR + '\\'

        current_date = datetime.today().strftime('%Y_%m_%d')

        response = self.get(urls.daily_treasury_par_yield_curve_rates)
        soup = BeautifulSoup(response.content, 'html.parser')

        csv_link = soup.find('div', attrs={'class': 'csv-feed views-data-export-feed'}).find('a').attrs.get('href')
        csv_file = self.get(csv_link)

        with open('{}{}_{}.csv'.format(output_path, scrapers.DTPYCR, current_date), 'w', encoding='utf-8') as file:
            file.write(csv_file.text)
            file.close()
        return common_keys.DOWNLOAD_SUCCESS

    def run_on_demand(self):
        for scraper in self.scrapers_dict.keys():
            if self.params.get(scraper).get(common_keys.ENABLE):
                print(scraper, self.scrapers_dict.get(scraper)(), end='\n\n')


if __name__ == '__main__':
    parameters = helpers.read_parameters('config_files/run_on_demand_conf.ini')
    proxy = helpers.read_proxy('config_files/proxy.ini')
    bot_scraper = BotScraper(parameters, proxy)
    bot_scraper.run_on_demand()
