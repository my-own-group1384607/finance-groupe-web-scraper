taux_de_change = f"https://www.bkam.ma/Marches/Principaux-indicateurs/Marche-des-changes/Cours-de-change/Cours-de" \
                 "-reference?date={}%2F{}%2F{}&block=cc51b5ce6878a3dc655dae26c47fddf8#address" \
                 "-5312b6def4ad0a94c5a992522868ac0a-cc51b5ce6878a3dc655dae26c47fddf8"

courbe_des_taux = f"https://www.bkam.ma/Marches/Principaux-indicateurs/Marche-obligataire/Marche-des-bons-de-tresor" \
                  "/Marche-secondaire/Taux-de-reference-des-bons-du-tresor?date={}%2F{}%2F{}&block" \
                  "=e1d6b9bbf87f86f8ba53e8518e882982#address-c3367fcefc5f524397748201aee5dab8" \
                  "-e1d6b9bbf87f86f8ba53e8518e882982"

taux_depot_a_terme = "https://www.bkam.ma/Marches/Taux-d-interet/Taux-des-depots-a-terme?month={}&year={}&block" \
                     "=03102740b6485a19ebc85c89f23ffc8a"

taux_compte_sur_carnet = "https://www.bkam.ma/Marches/Taux-d-interet/Taux-des-comptes-sur-carnet?startDate={}%2F{}" \
                         "%2F{}&lastFromDate={}%2F{}%2F{}&block=ff933bdb1ad2426a02cc8682577e808f#address" \
                         "-8e77b3bb422bb1fde8bad3a41a33987e-ff933bdb1ad2426a02cc8682577e808f"

taux_interet_debteur = "https://www.bkam.ma/Statistiques/Taux-d-interet/Taux-debiteurs?relationWith={}&startYear={" \
                       "}&endYear={}&block=3f927b0277dd9feff742550cbf315ebd "

inflation_et_inflation_sous_jacente = f"https://www.bkam.ma/Statistiques/Prix/Inflation-et-inflation-sous-jacente"

avoirs_officielles_de_reverses = f"https://www.bkam.ma/Statistiques/Avoirs-officiels-de-reserve"

intervention_bam = f"https://www.bkam.ma/Politique-monetaire/Cadre-operationnel/Dispositif-d-intervention-sur-le" \
                   "-marche-monetaire/Operations-principales?relationWith={}&startDate={}%2F{}%2F{}&endDate={}" \
                   "%2F{}%2F{}&block=93beb84adade119b669484a179643045#address-eaca65126c0ec271f3142a04f9a692e6" \
                   "-93beb84adade119b669484a179643045"

statistiques_monitaire = f"https://www.bkam.ma/Statistiques/Statistiques-monetaires/Series-statistiques-monetaires"

statistique_economique_financier = f"https://www.finances.gov.ma/fr/Pages/statistiques-economiques-financieres.aspx"

indicateur_mensuels_echanges_exterieurs_general_search = "https://www.oc.gov.ma/fr/publications?field_category_period" \
                                                         "icite_target_id=46&field_category_theme_target_id=57&filter" \
                                                         "_by_month_year=All&page={} "

indicateur_mensuels_echanges_exterieurs = "https://www.oc.gov.ma/fr/recherche?combine={}"

produit_interieur_prix_courant_prix_constant = f"https://applications-web.hcp.ma/DCN2022/CRegPIB.html"

courbe_des_taux_cemac = "https://www.beac.int/economie-stats/statistiques-titres-publics/"

operation_politique_monetaire_tunisie = "https://www.bct.gov.tn/bct/siteprod/tableau_statistique_q.jsp?params=PL203080"

situation_mensuelle_bct_tunisie = "https://www.bct.gov.tn/bct/siteprod/tableau_statistique.jsp?params=PL203010&cal=m&" \
                                  "page=P020&tab=020"

principaux_taux_interet_tunisie = "https://www.bct.gov.tn/bct/siteprod/stat_page.jsp?id=129"

balance_paiement_tunisie = "https://www.bct.gov.tn/bct/siteprod/tab_trimestriel.jsp?params=PL120010,PL120020," \
                           "PL120030&cal=t&page=P120&tab=040&pos=3 "

courbe_de_taux_egypte_bills = "https://www.cbe.org.eg/en/Auctions/Pages/AuctionsEGPTBillsHistorical.aspx"

courbe_de_taux_egypte_bounds = "https://www.cbe.org.eg/en/Auctions/Pages/AuctionsEGPTBonds.aspx"

courbe_de_taux_egypte_frd = "https://www.cbe.org.eg/en/Auctions/Pages/AuctionsFixedRateDepositsHistorical.aspx"

daily_interbank_historical_egypte = "https://www.cbe.org.eg/en/EconomicResearch/Statistics/Pages/DailyInterbankHistorical.aspx"

daily_treasury_par_yield_curve_rates = "https://home.treasury.gov/resource-center/data-chart-center/interest-rates/TextView?type=daily_treasury_yield_curve&field_tdr_date_value_month=202212"

getway_login = 'http://gateway:6380/login.htm'

getway_balance_pece = 'http://gateway:6380/2018/QUOTIDIEN/PCEC0100'

getway_dat = 'http://gateway:6380/2018/MENSUEL/DAT'
