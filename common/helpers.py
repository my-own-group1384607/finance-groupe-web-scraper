import ast
import configparser
import os

from common import scrapers, common_keys


def read_parameters(conf_file_path: str):
    config = configparser.ConfigParser()
    config.read(conf_file_path)

    for section in config.sections():
        assert section in scrapers.SCRAPERS

    params = dict()
    for section in config.sections():
        params[section] = {option: config.get(section, option) for option in config.options(section)}
        params[section][common_keys.ENABLE] = ast.literal_eval(params[section][common_keys.ENABLE])
    return params


def read_proxy(proxy_file_path: str):
    config = configparser.ConfigParser()
    try:
        config.read(proxy_file_path)
        assert len(config.sections()) > 0, "NO PROXY CONF FOUND IN FILE \'{}\'".format(proxy_file_path)
        proxy_section = config.sections()[0]
        return {option: config.get(proxy_section, option) for option in config.options(proxy_section)}
    except Exception as e:
        print(e.__str__())
        return {}


def create_folders_if_not_exists():
    # Create parent folder
    try:
        os.mkdir(common_keys.PARENT_PATH)
    except Exception as e:
        print(e.__str__())
    # Create folder for each scraper
    for scraper in scrapers.SCRAPERS:
        try:
            os.mkdir(common_keys.PARENT_PATH + scraper)
        except Exception as e:
            print(e.__str__())


def create_report_folders_if_not_exists():
    # Create parent folder
    try:
        os.mkdir(common_keys.PARENT_PATH + common_keys.REPORTS_FOLDERS)
    except Exception as e:
        print(e.__str__())
    # Create folder for each scraper
    for folder in [common_keys.DAILY_REPORT,
                   common_keys.WEEKLY_REPORT,
                   common_keys.MONTHLY_REPORT,
                   common_keys.QUARTERLY_REPORT]:
        try:
            os.mkdir(common_keys.PARENT_PATH + common_keys.REPORTS_FOLDERS + folder)
        except Exception as e:
            print(e.__str__())


def get_file_extension(filename):
    return filename.split('.')[-1]


def reset_intermediate_folder():
    try:
        os.rmdir(common_keys.INTERMEDIATE_PATH)
    except Exception as e:
        print(e.__str__())
    try:
        os.mkdir(common_keys.INTERMEDIATE_PATH)
    except Exception as e:
        print(e.__str__())


if __name__ == '__main__':
    create_folders_if_not_exists()
    create_report_folders_if_not_exists()
