from enum import Enum


class TDC:
    DATE = "date"
    OUTPUT_PATH = "output_path"


class CDT:
    DATE = "date"
    OUTPUT_PATH = "output_path"


class TDT:
    MONTH = "month"
    YEAR = "year"
    OUTPUT_PATH = "output_path"


class TCC:
    START_DATE = "start_date"
    END_DATE = "end_date"
    OUTPUT_PATH = "output_path"


class IB:
    INSTRUMENT = "instrument"
    START_DATE = "start_date"
    END_DATE = "end_date"
    OUTPUT_PATH = "output_path"


class TID:
    START_DATE = "start_date"
    END_DATE = "end_date"
    TRIMESTRE = "trimestre"
    OUTPUT_PATH = "output_path"
    ENCODE_T = {
        'T1': 59,
        'T2': 60,
        'T3': 61,
        'T4': 62,
        '': ''
    }


class IMEE:
    MONTH = "month"
    YEAR = "year"
    OUTPUT_PATH = "output_path"


class SM:
    OUTPUT_PATH = "output_path"


class SEF:
    OUTPUT_PATH = "output_path"


class PIPCPC:
    OUTPUT_PATH = "output_path"


class GETWAY_B_PCEC:
    DATE = "date"
    OUTPUT_PATH = "output_path"


class GETWAY_DAT:
    MONTH = "month"
    YEAR = "year"
    OUTPUT_PATH = "output_path"


class CDT_UMOA:
    OUTPUT_PATH = "output_path"
    DATE = "date"


class CDT_CEMAC:
    OUTPUT_PATH = "output_path"
    MONTH = "month"
    YEAR = "year"


class OPMT:
    OUTPUT_PATH = "output_path"


class SM_BCT_TUNISIE:
    OUTPUT_PATH = "output_path"


class PTIT:
    OUTPUT_PATH = "output_path"


class BPT:
    OUTPUT_PATH = "output_path"
    RECETTES = "EVOLUTION DES PRINCIPAUX FLUX ET SOLDES DES PAIEMENTS EXTERIEURS (RECETTES)"
    DEPENSES = "EVOLUTION DES PRINCIPAUX FLUX ET SOLDES DES PAIEMENTS EXTERIEURS (DÉPENSES)"
    SOLDES = "EVOLUTION DES PRINCIPAUX FLUX ET SOLDES DES PAIEMENTS EXTERIEURS (SOLDES)"
    CALENDRIER = "CALENDRIER D'ANNONCE DES DIFFUSIONS DE LA BALANCE DES PAIEMENTS"


class CDT_EGY_T_BILLS:
    START_DATE = "start_date"
    END_DATE = "end_date"
    OUTPUT_PATH = "output_path"


class CDT_EGY_T_BONDS:
    OUTPUT_PATH = "output_path"


class CDT_EGY_FRD:
    START_DATE = "start_date"
    END_DATE = "end_date"
    OUTPUT_PATH = "output_path"


class DIH:
    START_DATE = "start_date"
    END_DATE = "end_date"
    OUTPUT_PATH = "output_path"


class DTPYCR:
    OUTPUT_PATH = "output_path"


class IISJ:
    OUTPUT_PATH = "output_path"


class AOR:
    OUTPUT_PATH = "output_path"


class GETWAY_LOGIN:
    LOGIN = '00707707'
    PWD = '00707707'
