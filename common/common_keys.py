AVANCES_7_JOURS = "AVANCES_7_JOURS"
REPRISE_DE_LIQUIDITE_7_JOURS = "REPRISE_DE_LIQUIDITE_7_JOURS"
AUCUN_ENREGISTREMENT_TROUVE = 'Aucun enregistrement trouvé.'
INDECATOR_SEARCH_SENTENCE = "INDICATEURS DES ÉCHANGES EXTÉRIEURS À FIN {} {}"
DOWNLOAD_SUCCESS = "DOWNLOAD_SUCCESS"
DOWNLOAD_FAILED = "DOWNLOAD_FAILED"
FILE_NOT_FOUND = "FILE_NOT_FOUND"
MONTHS = {
    1: "Janvier", 2: "Février", 3: "Mars",
    4: "Avril", 5: "Mai", 6: "Juin",
    7: "Juillet", 8: "Août", 9: "Septembre",
    10: "Octobre", 11: "Novembre", 12: "Décembre",
}
# PARENT_PATH = "//nas-home-mly/FG_ART$/SCRAPED_DATA/"
PARENT_PATH = "C:\\Users\\im.ouelfaquir\\Documents\\SCRAPED_DATA\\"
INTERMEDIATE_PATH = "C:\\Users\\im.ouelfaquir\\Desktop\\finance-groupe-web-scraper\\data\\"
ENABLE = 'enable'
CHROME_DRIVER_PATH = "C:\\Users\\im.ouelfaquir\\Desktop\\finance-groupe-web-scraper\\web_driver\\chromedriver.exe"
RAPPORT_EXECUTION = '\\RAPPORT_EXECUTION'
PROXY = {
    "http": "10.10.211.234:8081",
    "https": "10.10.211.234:8081"
}
PROXY_CONFIG_FILE_PATH = 'C:\\Users\\im.ouelfaquir\\Desktop\\finance-groupe-web-scraper\\config_files\\proxy.ini'
REPORT_COLUMNS = [
    'date_execution', 'nom_scraper', 'result'
]

REPORTS_FOLDERS = 'REPORTS_FOLDERS\\'
DAILY_REPORT = 'DAILY_REPORT\\'
WEEKLY_REPORT = 'WEEKLY_REPORT\\'
MONTHLY_REPORT = 'MONTHLY_REPORT\\'
QUARTERLY_REPORT = 'QUARTERLY_REPORT\\'

# PROXY = {
#     "http": "10.10.211.234:8081",
#     "https": "10.10.211.234:8081"
# }

