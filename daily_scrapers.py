from datetime import datetime, timedelta

from bot_scraper import BotScraper
from common.scrapers import *
from common import common_keys, helpers

daily_scrapers = [
    TDC, CDT, OPMT, DIH, DTPYCR
]


def daily_runner():
    proxy = helpers.read_proxy(common_keys.PROXY_CONFIG_FILE_PATH)
    current_date = datetime.now()
    report = ''.join(column + ';' for column in common_keys.REPORT_COLUMNS)[:-1] + '\n'
    for scraper in daily_scrapers:
        params = {
            scraper: {'date': current_date.strftime('%d/%m/%Y'),
                      'output_path': '',  # common.PARENT_PATH + scraper + '/',
                      'start_date': (current_date - timedelta(days=1)).strftime('%d/%m/%Y'),
                      'end_date': current_date.strftime('%d/%m/%Y')}}

        bot_scraper = BotScraper(params, proxy)
        result = bot_scraper.scrapers_dict.get(scraper)()
        print(scraper, result)
        report += '{};{};{}\n'.format(datetime.now(), scraper, result)

    with open('{}{}{}rapport_execution_{}.txt'.format(common_keys.PARENT_PATH,
                                                      common_keys.REPORTS_FOLDERS,
                                                      common_keys.DAILY_REPORT,
                                                      current_date.strftime('%Y-%m-%d_%Hh%Mm%Ss')), 'w') as file:
        file.write(report)
        file.close()


if __name__ == '__main__':
    daily_runner()
