from bot_scraper import BotScraper
from common import helpers, common_keys
import sys

if __name__ == '__main__':
    try:
        conf_file = sys.argv[1]
    except Exception as e:
        raise Exception("Config file si missing\nTry : python run_on_demand.py < config_file.ini >")

    parameters = helpers.read_parameters(sys.argv[1])
    proxy = helpers.read_proxy(common_keys.PROXY_CONFIG_FILE_PATH)
    bot_scraper = BotScraper(parameters, proxy)
    bot_scraper.run_on_demand()

