import time
from datetime import timedelta, datetime

import requests
from selenium.webdriver.common.by import By

from browser import init_web_driver
from common.params import *
from bot_scraper import BotScraper
from common import scrapers, common_keys, urls


def save_logs(logs: str, path: str, output_filename: str):
    with open(path + output_filename, 'a') as success_logs_file:
        success_logs_file.write(logs)
        success_logs_file.close()


def cours_de_change_et_courbe_des_taux_from_a_given_date(from_date, to_date, output_path, scraper=scrapers.TDC,
                                                         max_retries=5):
    current_date = datetime.strptime(from_date, "%d/%m/%Y")
    to_date = datetime.strptime(to_date, "%d/%m/%Y")
    download_failed_logs, file_not_found_logs, download_success_logs = '', '', ''
    prm = {
        scrapers.TDC: {TDC.DATE: current_date.strftime("%d/%m/%Y"), TDC.OUTPUT_PATH: output_path}}

    while current_date < to_date:
        try:
            with open('{}{}_{}.csv'.format(output_path, scraper, current_date.strftime("%Y-%m-%d"), 'r')) as file:
                download_success_logs += "{} {}\n".format(current_date, common_keys.DOWNLOAD_SUCCESS)
                print("{} {} - FOUND\n".format(current_date, common_keys.DOWNLOAD_SUCCESS))
        except:
            number_of_tries = 0
            download_status = BotScraper(prm).cours_de_change_et_courbe_des_taux(scraper)
            while number_of_tries < max_retries and download_status is common_keys.DOWNLOAD_FAILED:
                download_status = BotScraper(prm).cours_de_change_et_courbe_des_taux(scraper)
                number_of_tries += 1

            print("{} {}\n".format(current_date, download_status))

            if download_status is common_keys.DOWNLOAD_SUCCESS:
                download_success_logs += "{} {}\n".format(current_date, download_status)
            elif download_status is common_keys.FILE_NOT_FOUND:
                file_not_found_logs += "{} {}\n".format(current_date, download_status)
            elif download_status is common_keys.DOWNLOAD_FAILED:
                download_failed_logs += "{} {}\n".format(current_date, download_status)

        current_date += timedelta(days=1)
        prm[scrapers.TDC][TDC.DATE] = current_date.strftime("%d/%m/%Y")

    save_logs(download_success_logs, prm[scrapers.TDC][TDC.OUTPUT_PATH], "success.txt")
    save_logs(file_not_found_logs, prm[scrapers.TDC][TDC.OUTPUT_PATH], "not_found.txt")
    save_logs(download_failed_logs, prm[scrapers.TDC][TDC.OUTPUT_PATH], "failed.txt")


def taux_depot_a_terme_from_a_given_date(form_year, to_year, output_path, max_retries=5):
    download_failed_logs, file_not_found_logs, download_success_logs = '', '', ''
    prm = dict()
    for year in range(form_year, to_year + 1):
        for month in range(1, 13):
            prm = {
                scrapers.TDT: {TDT.MONTH: month, TDT.YEAR: year, TDT.OUTPUT_PATH: output_path}}
            number_of_tries = 0
            download_status = BotScraper(prm).taux_depot_a_terme()
            while number_of_tries < max_retries and download_status is common_keys.DOWNLOAD_FAILED:
                download_status = BotScraper(prm).cours_de_change_et_courbe_des_taux()
                number_of_tries += 1

            print("{}-{} {}\n".format(year, month, download_status))

            if download_status is common_keys.DOWNLOAD_SUCCESS:
                download_success_logs += "{}-{} {}\n".format(year, month, download_status)
            elif download_status is common_keys.FILE_NOT_FOUND:
                file_not_found_logs += "{}-{} {}\n".format(year, month, download_status)
            elif download_status is common_keys.DOWNLOAD_FAILED:
                download_failed_logs += "{}-{} {}\n".format(year, month, download_status)

    save_logs(download_success_logs, prm[scrapers.TDT][TDT.OUTPUT_PATH], "success.txt")
    save_logs(file_not_found_logs, prm[scrapers.TDT][TDT.OUTPUT_PATH], "not_found.txt")
    save_logs(download_failed_logs, prm[scrapers.TDT][TDT.OUTPUT_PATH], "failed.txt")


def indicateur_mensuels_echanges_exterieurs_from_given_date(form_year, to_year, output_path, max_retries=5):
    download_failed_logs, file_not_found_logs, download_success_logs = '', '', ''
    prm = dict()
    for year in range(form_year, to_year + 1):
        for month in range(1, 13):
            prm = {
                scrapers.IMEE: {IMEE.MONTH: month, IMEE.YEAR: year, IMEE.OUTPUT_PATH: output_path}}
            number_of_tries = 0
            download_status = BotScraper(prm).indicateur_mensuels_echanges_exterieurs()
            while number_of_tries < max_retries and download_status is common_keys.DOWNLOAD_FAILED:
                download_status = BotScraper(prm).indicateur_mensuels_echanges_exterieurs()
                number_of_tries += 1

            print("{}-{} {}\n".format(year, month, download_status))

            if download_status is common_keys.DOWNLOAD_SUCCESS:
                download_success_logs += "{}-{} {}\n".format(year, month, download_status)
            elif download_status is common_keys.FILE_NOT_FOUND:
                file_not_found_logs += "{}-{} {}\n".format(year, month, download_status)
            elif download_status is common_keys.DOWNLOAD_FAILED:
                download_failed_logs += "{}-{} {}\n".format(year, month, download_status)

    save_logs(download_success_logs, prm[scrapers.IMEE][IMEE.OUTPUT_PATH], "success.txt")
    save_logs(file_not_found_logs, prm[scrapers.IMEE][IMEE.OUTPUT_PATH], "not_found.txt")
    save_logs(download_failed_logs, prm[scrapers.IMEE][IMEE.OUTPUT_PATH], "failed.txt")


def taux_interet_debiteur_load_from_given_date(start_year, end_year, output_path, max_retries=5):
    download_failed_logs, file_not_found_logs, download_success_logs = '', '', ''
    prm = dict()
    for year in range(start_year, end_year + 1):
        for trimestre in range(1, 5):
            prm = {
                scrapers.TID: {TID.START_DATE: year, TID.END_DATE: year, TID.TRIMESTRE: 'T{}'.format(trimestre),
                               TID.OUTPUT_PATH: output_path}}
            number_of_tries = 0
            download_status = BotScraper(prm).taux_interet_debiteur()
            while number_of_tries < max_retries and download_status is common_keys.DOWNLOAD_FAILED:
                download_status = BotScraper(prm).taux_depot_a_terme()
                number_of_tries += 1

            print("{}-{}-{} {}\n".format(year, year, 'T{}'.format(trimestre), download_status))

            if download_status is common_keys.DOWNLOAD_SUCCESS:
                download_success_logs += "{}-{}-{} {}\n".format(year, year, 'T{}'.format(trimestre), download_status)
            elif download_status is common_keys.FILE_NOT_FOUND:
                file_not_found_logs += "{}-{}-{} {}\n".format(year, year, 'T{}'.format(trimestre), download_status)
            elif download_status is common_keys.DOWNLOAD_FAILED:
                download_failed_logs += "{}-{}-{} {}\n".format(year, year, 'T{}'.format(trimestre), download_status)

    save_logs(download_success_logs, prm[scrapers.TID][TID.OUTPUT_PATH], "success.txt")
    save_logs(file_not_found_logs, prm[scrapers.TID][TID.OUTPUT_PATH], "not_found.txt")
    save_logs(download_failed_logs, prm[scrapers.TID][TID.OUTPUT_PATH], "failed.txt")


def courbe_des_taux_cemac(output_path):

    driver = init_web_driver(output_path, True)
    driver.get(urls.courbe_des_taux_cemac)

    for i in range(19):
        time.sleep(1)
        table = driver.find_element(By.ID, 'documents_contenu_cpt')
        for line in table.find_elements(By.TAG_NAME, 'a'):
            print(line.text, line.get_attribute('href'))
            pdf_file = requests.get(line.get_attribute('href'), proxies=common_keys.PROXY, verify=False)
            with open('{}{}.pdf'.format(output_path, line.text), 'wb') as file:
                file.write(pdf_file.content)
                file.close()
        next_button = driver.find_element(By.ID, 'documents_contenu_cpt_next')
        next_button.click()

    time.sleep(20)


if __name__ == '__main__':
    # start_time = time.time()
    # taux_interet_debiteur_load_from_given_date(2015,
    #                                            2022,
    #                                            common.PARENT_PATH + scrapers.TID + '/',
    #                                            max_retries=10)
    # print("Courbe des taux:  %s seconds ---" % (time.time() - start_time))
    courbe_des_taux_cemac(common_keys.PARENT_PATH + scrapers.CDT_CEMAC)

