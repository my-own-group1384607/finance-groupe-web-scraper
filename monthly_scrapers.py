from datetime import datetime, timedelta

from bot_scraper import BotScraper
from common.scrapers import *
from common import common_keys, helpers

monthly_scrapers = [
    TDT, SM, SEF, IMEE, CDT_CEMAC, SM_BCT_TUNISIE, PTIT
]


def monthly_runner():
    proxy = helpers.read_proxy(common_keys.PROXY_CONFIG_FILE_PATH)
    current_date = datetime.now()
    report = ''.join(column + ';' for column in common_keys.REPORT_COLUMNS)[:-1] + '\n'
    for scraper in monthly_scrapers:
        params = {
            scraper: {
                'output_path': '',
                'month': str(current_date.month),
                'year': str(current_date.year)}}

        bot_scraper = BotScraper(params, proxy)
        result = bot_scraper.scrapers_dict.get(scraper)()
        print(scraper, result)
        report += '{};{};{}\n'.format(datetime.now(), scraper, result)

    with open('{}{}{}rapport_execution_{}.txt'.format(common_keys.PARENT_PATH,
                                                      common_keys.REPORTS_FOLDERS,
                                                      common_keys.MONTHLY_REPORT,
                                                      current_date.strftime('%Y-%m-%d_%Hh%Mm%Ss')), 'w') as file:
        file.write(report)
        file.close()


if __name__ == '__main__':
    monthly_runner()
