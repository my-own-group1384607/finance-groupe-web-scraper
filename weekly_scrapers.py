from datetime import datetime, timedelta

from bot_scraper import BotScraper
from common.scrapers import *
from common import common_keys, helpers


def get_friday_date_last_week(current_date: datetime):
    while current_date.weekday() != 4:
        current_date += timedelta(days=-1)
    return current_date


def get_first_day_of_week_datek(current_date: datetime):
    while current_date.weekday() != 0:
        current_date += timedelta(days=-1)
    return current_date


def get_last_day_of_week_datek(current_date: datetime):
    while current_date.weekday() != 6:
        current_date += timedelta(days=1)
    return current_date


weekly_scrapers = [
    IISJ, AOR, CDT_UMOA, CDT_EGY_FRD, CDT_EGY_T_BONDS, CDT_EGY_T_BILLS
]


def weekly_runner():
    proxy = helpers.read_proxy(common_keys.PROXY_CONFIG_FILE_PATH)
    current_date = datetime.now()
    report = ''.join(column + ';' for column in common_keys.REPORT_COLUMNS)[:-1] + '\n'
    for scraper in weekly_scrapers:
        params = {
            scraper: {
                'output_path': '',
                'date': get_friday_date_last_week(current_date).strftime('%d/%m/%Y'),
                'start_date': get_first_day_of_week_datek(current_date).strftime('%d/%m/%Y'),
                'end_date': get_friday_date_last_week(current_date).strftime('%d/%m/%Y')
            }}

        bot_scraper = BotScraper(params, proxy)
        result = bot_scraper.scrapers_dict.get(scraper)()
        print(scraper, result)
        report += '{};{};{}\n'.format(current_date, scraper, result)

    with open('{}{}{}rapport_execution_{}.txt'.format(common_keys.PARENT_PATH,
                                                      common_keys.REPORTS_FOLDERS,
                                                      common_keys.WEEKLY_REPORT,
                                                      datetime.now().strftime('%Y-%m-%d_%Hh%Mm%Ss')), 'w') as file:
        file.write(report)
        file.close()


if __name__ == '__main__':
    weekly_runner()
