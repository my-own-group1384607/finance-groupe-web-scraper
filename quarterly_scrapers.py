from datetime import datetime

from bot_scraper import BotScraper
from common.scrapers import *
from common import common_keys, helpers


quarterly_scrapers = [
    TID, BPT
]


def quarterly_runner():
    proxy = helpers.read_proxy(common_keys.PROXY_CONFIG_FILE_PATH)
    current_date = datetime.now()
    report = ''.join(column + ';' for column in common_keys.REPORT_COLUMNS)[:-1] + '\n'
    for scraper in quarterly_scrapers:
        quarter = (current_date.month - 1) // 3 + 1
        params = {
            scraper: {
                'output_path': '',
                'trimestre': f'T{quarter}',
                'start_date': current_date.year,
                'end_date': current_date.year
            }}

        bot_scraper = BotScraper(params, proxy)
        result = bot_scraper.scrapers_dict.get(scraper)()
        print(scraper, result)
        report += '{};{};{}\n'.format(current_date, scraper, result)

    with open('{}{}{}rapport_execution_{}.txt'.format(common_keys.PARENT_PATH,
                                                      common_keys.REPORTS_FOLDERS,
                                                      common_keys.QUARTERLY_REPORT,
                                                      datetime.now().strftime('%Y-%m-%d_%Hh%Mm%Ss')), 'w') as file:
        file.write(report)
        file.close()


if __name__ == '__main__':
    quarterly_runner()
