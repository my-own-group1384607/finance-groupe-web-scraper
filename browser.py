from selenium import webdriver
from common import common_keys


def init_web_driver(output_path, visible=False):
    prefs = {
        'download.default_directory': output_path,
        "profile.default_content_settings.popups": 0,
        "download.prompt_for_download": False,  # To auto download the file
        "plugins.always_open_pdf_externally": True,  # It will not show PDF directly in chrome
        "directory_upgrade": True
    }
    options = webdriver.ChromeOptions()
    options.add_experimental_option('prefs', prefs)
    if not visible:
        options.add_argument("--headless")
        options.add_argument("window-size=1920,1080")

    driver = webdriver.Chrome(common_keys.CHROME_DRIVER_PATH, options=options)
    driver.set_page_load_timeout(120)
    driver.set_script_timeout(120)
    driver.maximize_window()
    return driver
